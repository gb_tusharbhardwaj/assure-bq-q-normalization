{{ config(
    cluster_by = "_airbyte_emitted_at",
    partition_by = {"field": "_airbyte_emitted_at", "data_type": "timestamp", "granularity": "day"},
    unique_key = '_airbyte_ab_id',
    schema = "_airbyte_public",
    tags = [ "top-level-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('dev_assure_bq_q_ab2') }}
select
    {{ dbt_utils.surrogate_key([
        'id',
        'item_return_order_item_status',
        'item_is_excess',
        'item_qc_status',
        'item_return_reason',
        'item_item_code',
        'item_channel_sku_code',
        'item_selling_price_per_unit',
        'item_virtual_sku_child_item_return_postings',
        'item_qc_reason',
        'return_creation_time',
        'partner_code',
        'message_id',
        'consignment_number',
        'forward_order_code',
        'awb_number',
        'partner_location_code',
        'return_order_code',
        'event_time',
        'payment_method',
        'shipping_address_zip',
        'shipping_address_country',
        'shipping_address_phone',
        'shipping_address_city',
        'shipping_address_name',
        'shipping_address_state',
        'shipping_address_line3',
        'shipping_address_line2',
        'shipping_address_line1',
        'shipping_address_email',
        'channel_name',
        'billing_address_zip',
        'billing_address_country',
        'billing_address_phone',
        'billing_address_city',
        'billing_address_name',
        'billing_address_state',
        'billing_address_line3',
        'billing_address_line2',
        'billing_address_line1',
        'billing_address_email',
        'location_code',
        'return_order_type',
        'task',
        'status',
        'api',
        'created_at',
        'pk',
        'source',
        'uuid',
        'attributes',
    ]) }} as _airbyte_dev_assure_bq_q_hashid,
    tmp.*
from {{ ref('dev_assure_bq_q_ab2') }} tmp
-- dev_assure_bq_q
where 1 = 1
{{ incremental_clause('_airbyte_emitted_at') }}

